package kdr;

import java.util.ArrayList;
import java.util.Random;
import kdr.Ladung;

public class Raumschiffe {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsName;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsVerzeichnis;
	
	public Raumschiffe() {
		
	}
	
	public Raumschiffe(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandschildeInProzent,
			int zustandhuelleInProzent, int zustandlebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsName) {
		super();
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandschildeInProzent;
		this.huelleInProzent = zustandhuelleInProzent;
		this.lebenserhaltungssystemeInProzent = zustandlebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsName = schiffsName;
	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int zustandenergieversorgungInProzentNeu) {
		this.energieversorgungInProzent = zustandenergieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int zustandschildeInProzentNeu) {
		this.schildeInProzent = zustandschildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int zustandhuelleInProzentNeu) {
		this.huelleInProzent = zustandhuelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsName() {
		return schiffsName;
	}

	public void setSchiffsName(String schiffsName) {
		this.schiffsName = schiffsName;
	}
	public void addLadung(Ladung neueLadung) {
		
	}
	public void photonentorpedoschiessen(Raumschiffe r) {
		if (getPhotonentorpedoAnzahl() <= 0) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
				setPhotonentorpedoAnzahl(photonentorpedoAnzahl - 1);
				nachrichtAnAlle("Photonentorpedos abgeschossen");
				treffer(r);
			}
	}
	public void phaserkanoneschiessen(Raumschiffe r) {
		if (getEnergieversorgungInProzent() < 50) {
			nachrichtAnAlle("-=*Click*=-");
		}
		else {
			setEnergieversorgungInProzent(energieversorgungInProzent - 50);
			treffer(r);
		}
	}
	private void treffer(Raumschiffe r) {
		setSchildeInProzent(this.schildeInProzent =- 50);
		if (this.schildeInProzent == 0) {
			this.huelleInProzent =- 50;
			this.energieversorgungInProzent =- 50;
		}
		if (this.huelleInProzent == 0) {
			this.lebenserhaltungssystemeInProzent = 0;
			nachrichtAnAlle("Lebenserhaltungssysteme wurden vollkommen zerst�rt.");
		}
		
	}
	public void nachrichtAnAlle(String message) {
		broadcastKommunikator.add(message);
	}
	public ArrayList<String> eintraegeLogbuchZurueckgeben(){
		return broadcastKommunikator;
		
	}
	public void photonentorpedosLaden(int anzahlTorpedos) {
		
	}
	public void reperaturDurchfuehren(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
		if (schutzschilde == false) {
			Random rand = new Random();
			int n =rand.nextInt(100);
			n += 1;
		}
		else {
			// go on 
		}
		
		if (energieversorgung == false) {
			
		}
		else {
			// go on
		}
		
		if (schiffshuelle == false) {
			
		}
		else {
			// go on 
		}
	}
	public void zustandRaumschiff() {
		broadcastKommunikator.add(Integer.toString(getSchildeInProzent()));
		broadcastKommunikator.add(Integer.toString(getHuelleInProzent()));
		broadcastKommunikator.add(Integer.toString(getLebenserhaltungssystemeInProzent()));
		broadcastKommunikator.add(Integer.toString(getEnergieversorgungInProzent()));
		
		System.out.println(broadcastKommunikator);
	}
	public void ladungsverzeichnisAusgeben() {
		if (this.ladungsVerzeichnis.size() == 0) {
			System.out.println("Es ist keine Ladung auf dem Schiff.");
		}
		else {
			for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
				System.out.println(this.ladungsVerzeichnis.get(i).getBezeichnung() + ": " + this.ladungsVerzeichnis.get(i).getMenge());
			}
		}
	}
	public void ladungsverzeichnisAufraeumen() {
		for (int i = 0; i < this.ladungsVerzeichnis.size(); i++) {
			if (this.ladungsVerzeichnis.get(i).getMenge() == 0) {
				this.ladungsVerzeichnis.remove(i);
			}
		}
		
	}
}

	
