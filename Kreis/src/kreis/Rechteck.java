package kreis;

import java.awt.Point;

public class Rechteck {
	private double a;
	private double b;
    
	public Rechteck(double a, double b)
	{
	  seta(a);
	  setb(b);
	}
	
	public void seta(double a) {
		if(a > 0)
			this.a = a;
		else
			this.a = 0;
	}
	
	public void setb(double b) {
		if(b > 0)
			this.b = b;
		else
			this.b = 0;
	}
	
	public double geta() {
		return this.a;
	}
	
	public double getb() {
		return this.b;
	}
	
	public double getUmfang() {
		return (this.a + this.b) * 2;
	}
	
	public double getFlaeche() {
		return this.a * this.b;
	}
	
	public double getDiagonal() {
		return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
	}
	
}
